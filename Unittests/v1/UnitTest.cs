﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests.V1
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Monosoft.EmailNotification.V1.DTO;
    using Monosoft.Microservice.V1.Commands;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// SaveSMTPSettings
        /// </summary>
        [Test]
        public void SaveSMTPSettings()
        {
            var smptSettings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            Settings.SaveSettings(smptSettings);
            var loaded = Settings.LoadSettings();
            Assert.AreEqual(smptSettings.smtpUserName, loaded.smtpUserName, "Loaded settings should be the same as saved settings");
            Assert.AreEqual(smptSettings.smtpPassword, loaded.smtpPassword, "Loaded settings should be the same as saved settings");
            Assert.AreEqual(smptSettings.smtpServerName, loaded.smtpServerName, "Loaded settings should be the same as saved settings");
            Assert.AreEqual(smptSettings.smtpPort, loaded.smtpPort, "Loaded settings should be the same as saved settings");
        }

        /// <summary>
        /// SMTPSettingsWithOutSMTPPassword
        /// </summary>
        [Test]
        public void SMTPSettingsWithOutSMTPPassword()
        {
            var smtpSettings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = " ", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => Settings.SaveSettings(smtpSettings));
            Assert.AreEqual("The SMTPPassword is empty", ex.Message);
        }

        /// <summary>
        /// SMTPSettingsWithOutSMTPServerName
        /// </summary>
        [Test]
        public void SMTPSettingsWithOutSMTPServerName()
        {
            var smtpSettings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = " ", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => Settings.SaveSettings(smtpSettings));
            Assert.AreEqual("The SMTPServerName is empty", ex.Message);
        }

        /// <summary>
        /// SMTPSettingsWithOutSMTPUserName
        /// </summary>
        [Test]
        public void SMTPSettingsWithOutSMTPUserName()
        {
            var smtpSettings = new Settings() { smtpUserName = " ", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => Settings.SaveSettings(smtpSettings));
            Assert.AreEqual("The SMTPUserName is empty or not a valid email", ex.Message);
        }

        /// <summary>
        /// SMTPSettingsNotValidSMTPUserName
        /// </summary>
        [Test]
        public void SMTPSettingsNotValidSMTPUserName()
        {
            var smtpSettings = new Settings() { smtpUserName = "testmail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => Settings.SaveSettings(smtpSettings));
            Assert.AreEqual("The SMTPUserName is empty or not a valid email", ex.Message);
        }

        /// <summary>
        /// SMTPSettingsWithOutSMTPPort
        /// </summary>
        [Test]
        public void SMTPSettingsWithOutSMTPPort()
        {
            var smtpSettings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com" };
            var ex = Assert.Throws<ValidationException>(() => Settings.SaveSettings(smtpSettings));
            Assert.AreEqual("The SMTPPort is empty", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutSender
        /// </summary>
        [Test]
        public void SendEmailWithOutSender()
        {
            var receiver = "test@mail.dk";
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = string.Empty, receivers = new List<string> { receiver }, title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("Sender is empty or not a valid email", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutNameOfSender
        /// </summary>
        [Test]
        public void SendEmailWithOutNameOfSender()
        {
            var receiver = "test@mail.dk";
            var smtpmsg = new EmailNotification() { nameOfSender = " ", sender = "test@mail.dk", receivers = new List<string> { receiver }, title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("NameOfSender is empty", ex.Message);
        }

        /// <summary>
        /// SendEmailWithNullReferenceToListOfReceivers
        /// </summary>
        [Test]
        public void SendEmailWithNullReferenceToListOfReceivers()
        {
            var smtpmsg = new EmailNotification() { nameOfSender = "test", sender = "mikael@live.dk", title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("The list of receivers is empty", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutAnyReceiverButWithNewList
        /// </summary>
        [Test]
        public void SendEmailWithOutAnyReceiverButWithNewList()
        {
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = "test@mail.com", receivers = new List<string>(), title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("You have not add any receiver", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutReceiver
        /// </summary>
        [Test]
        public void SendEmailWithOutReceiver()
        {
            var receiver = " ";
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = "test@mail.dk", receivers = new List<string> { receiver }, title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("One of the email you have add is empty or not a valid email", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutValidEmail
        /// </summary>
        [Test]
        public void SendEmailWithOutValidEmail()
        {
            var receiver = "testmail.dk";
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = "test@mail.dk", receivers = new List<string> { receiver }, title = "title test", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("One of the email you have add is empty or not a valid email", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutTitle
        /// </summary>
        [Test]
        public void SendEmailWithOutTitle()
        {
            var receiver = "test@mail.dk";
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = "test@mail.dk", receivers = new List<string> { receiver }, title = " ", message = "hej med dig" };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("Title is empty", ex.Message);
        }

        /// <summary>
        /// SendEmailWithOutMessage
        /// </summary>
        [Test]
        public void SendEmailWithOutMessage()
        {
            var receiver = "test@mail.dk";
            var smtpmsg = new EmailNotification() { nameOfSender = "test name", sender = "test@mail.dk", receivers = new List<string> { receiver }, title = "title test", message = " " };
            var settings = new Settings() { smtpUserName = "test@mail.com", smtpPassword = "password", smtpServerName = "smtp.gmail.com", smtpPort = 465 };
            var ex = Assert.Throws<ValidationException>(() => EmailSend.SendEmail(smtpmsg, settings));
            Assert.AreEqual("Message is empty", ex.Message);
        }
    }
}
