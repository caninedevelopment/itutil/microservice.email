﻿
namespace Monosoft.Microservice
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new V2.Commands.Namespace(),
            });
        }
    }
}
