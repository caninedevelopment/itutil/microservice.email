﻿// <copyright file="SmtpCheck.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Notification.V1
{
    using System;
    using System.IO;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Text;

    /// <summary>
    /// SmtpCheck
    /// </summary>
    public static class SmtpCheck
    {
        /// <summary>
        /// CheckIfSmtpWorking
        /// </summary>
        /// <param name="smtpServer">string</param>
        /// <param name="serverPort">int</param>
        /// <returns>bool</returns>
        public static bool CheckIfSmtpWorking(string smtpServer, int serverPort)
        {
            using (var client = new TcpClient())
            {
                var server = smtpServer;
                var port = serverPort;
                client.Connect(server, port);

                // As GMail requires SSL we should use SslStream
                // If your SMTP server doesn't support SSL you can
                // work directly with the underlying stream
                using (var stream = client.GetStream())
                using (var sslStream = new SslStream(stream))
                {
                    sslStream.AuthenticateAsClient(server);
                    using (var writer = new StreamWriter(sslStream))
                    using (var reader = new StreamReader(sslStream))
                    {
                        writer.WriteLine("EHLO " + server);
                        writer.Flush();
                        StringBuilder sb = new StringBuilder();
                        var s = reader.ReadLine();
                        var test = s.Substring(0, s.IndexOf(" ", StringComparison.OrdinalIgnoreCase));
                        for (int i = 0; i < test.Length; i++)
                        {
                            sb.Append(test[i]);
                        }

                        int num = int.Parse(sb.ToString()/*, ITUtil.Common.Constants.Culture.DefaultCulture*/);
                        if (num > 200 && num < 300)
                        {
                            return true;
                        }

                        return false;

                        // GMail responds with: 220 mx.google.com ESMTP
                    }
                }
            }
        }
    }
}