﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Microservice.V1.Commands
{
    using ITUtil.Common.Command;

    /// <summary>
    /// Controller
    /// </summary>
    public class Namespace : BaseNamespace
    {
        private const string Servicename = "EmailNotification";
        internal static readonly Claim AdminClaim = new Claim()
        {
            key = "administrator",
            dataContext = Claim.DataContextEnum.organisationClaims,
            description = new ITUtil.Common.Base.LocalizedString[] { new ITUtil.Common.Base.LocalizedString("en", "User is allowed to set settings or see them") }
        };

        public Namespace() : base("EmailNotification", new ITUtil.Common.Command.ProgramVersion("1.0.0.0"))
        {
            this.Commands.Add(new Email());
            this.commands.Add(new GetSmtpsettings());
            this.Commands.Add(new SetSmtpsettings());
        }
    }
}
