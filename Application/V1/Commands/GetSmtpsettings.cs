﻿using Monosoft.EmailNotification.V1.DTO;

namespace Monosoft.Microservice.V1.Commands
{
    public class GetSmtpsettings : ITUtil.Common.Command.GetCommand<object, EmailNotification.V1.DTO.Settings>
    {
        public GetSmtpsettings()
            : base("Get the current SMTP settings")
        {
            this.Claims.Add(Namespace.AdminClaim);
        }

        public override Settings Execute(object input)
        {
            return EmailNotification.V1.DTO.Settings.LoadSettings();
        }

    }
}