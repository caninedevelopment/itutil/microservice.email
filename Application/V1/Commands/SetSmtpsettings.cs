﻿namespace Monosoft.Microservice.V1.Commands
{
    public class SetSmtpsettings : ITUtil.Common.Command.UpdateCommand<Monosoft.EmailNotification.V1.DTO.Settings>
    {
        public SetSmtpsettings()
            : base("Get the current SMTP settings")
        {
            this.Claims.Add(Namespace.AdminClaim);
        }

        public override void Execute(Monosoft.EmailNotification.V1.DTO.Settings input)
        {
            EmailNotification.V1.DTO.Settings.SaveSettings(input);
        }
    }
}