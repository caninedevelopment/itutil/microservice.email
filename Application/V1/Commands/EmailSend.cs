﻿// <copyright file="EmailSend.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Microservice.V1.Commands
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using MailKit.Net.Smtp;
    using MimeKit;

    public class Email : ITUtil.Common.Command.InsertCommand<Monosoft.EmailNotification.V1.DTO.EmailNotification>
    {
        public Email()
            : base("Sends an email")
        {
        }

        public override void Execute(Monosoft.EmailNotification.V1.DTO.EmailNotification input)
        {
            Monosoft.EmailNotification.V1.DTO.Settings settings = EmailNotification.V1.DTO.Settings.LoadSettings();
            EmailSend.SendEmail(input, settings);
        }
    }

    /// <summary>
    /// EmailSend
    /// </summary>
    public static class EmailSend
    {
        /// <summary>
        /// SendEmail
        /// </summary>
        /// <param name="smtpmsg">EmailNotification</param>
        /// <param name="settings">Settings</param>
        /// <returns>bool</returns>
        public static void SendEmail(Monosoft.EmailNotification.V1.DTO.EmailNotification smtpmsg, Monosoft.EmailNotification.V1.DTO.Settings settings)
        {
            if (smtpmsg == null)
            {
                throw new ArgumentNullException(nameof(smtpmsg));
            }

            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            if (string.IsNullOrEmpty(smtpmsg.nameOfSender) || smtpmsg.nameOfSender.Trim().Length == 0)
            {
                throw new ValidationException("NameOfSender is empty");
            }

            if (string.IsNullOrEmpty(smtpmsg.sender) || !smtpmsg.sender.Contains("@", StringComparison.OrdinalIgnoreCase) || smtpmsg.sender.Trim().Length == 0)
            {
                throw new ValidationException("Sender is empty or not a valid email");
            }

            if (smtpmsg.receivers == null)
            {
                throw new ValidationException("The list of receivers is empty");
            }

            if (smtpmsg.receivers != null)
            {
                if (smtpmsg.receivers.Count == 0)
                {
                    throw new ValidationException("You have not add any receiver");
                }

                for (int i = 0; i < smtpmsg.receivers.Count; i++)
                {
                    if (string.IsNullOrEmpty(smtpmsg.receivers[i]) || !smtpmsg.receivers[i].Contains("@", StringComparison.OrdinalIgnoreCase) || smtpmsg.receivers[i].Trim().Length == 0)
                    {
                        throw new ValidationException("One of the email you have add is empty or not a valid email");
                    }
                }
            }

            if (string.IsNullOrEmpty(smtpmsg.title) || smtpmsg.title.Trim().Length == 0)
            {
                throw new ValidationException("Title is empty");
            }

            if (string.IsNullOrEmpty(smtpmsg.message) || smtpmsg.message.Trim().Length == 0)
            {
                throw new ValidationException("Message is empty");
            }

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(smtpmsg.nameOfSender, smtpmsg.sender));
            foreach (var receiver in smtpmsg.receivers)
            {
                message.To.Add(new MailboxAddress(receiver));
            }

            message.Subject = smtpmsg.title;

            message.Body = new TextPart("html")
            {
                Text = smtpmsg.message,
            };

            using (var client = new SmtpClient())
            {
                client.Connect(settings.smtpServerName, settings.smtpPort);
                client.Authenticate(settings.smtpUserName, settings.smtpPassword);
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}
