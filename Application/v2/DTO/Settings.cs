﻿// <copyright file="Settings.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.EmailNotification.V2.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Settings
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class Settings
    {
        /// <summary>
        /// Gets or sets name
        /// </summary>
        public int smtpPort { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpServerName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpUserName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpPassword { get; set; }

        /// <summary>
        /// settingsfile
        /// </summary>
        private static string settingsfile = "notification.json";

        /// <summary>
        /// Gets or sets the current/active diagnostics settings.
        /// </summary>
        /// <returns>
        /// retun settings as json.
        /// </returns>
        public static V2.DTO.Settings LoadSettings()
        {
            if (System.IO.File.Exists(settingsfile))
            {
                var json = System.IO.File.ReadAllText(settingsfile);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<V2.DTO.Settings>(json);
            }
            else
            {
                return new V2.DTO.Settings();
            }
        }

        /// <summary>
        /// Gets or sets the current/active diagnostics settings.
        /// </summary>
        /// <param name="value">Settings</param>
        public static void SaveSettings(V2.DTO.Settings value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            if (string.IsNullOrEmpty(value.smtpPassword.Trim()))
            {
                throw new ValidationException("The SMTPPassword is empty");
            }

            if (string.IsNullOrEmpty(value.smtpServerName.Trim()))
            {
                throw new ValidationException("The SMTPServerName is empty");
            }

            if (string.IsNullOrEmpty(value.smtpUserName.Trim()) || !value.smtpUserName.Contains("@", StringComparison.OrdinalIgnoreCase))
            {
                throw new ValidationException("The SMTPUserName is empty or not a valid email");
            }

            if (value.smtpPort == 0)
            {
                throw new ValidationException("The SMTPPort is empty");
            }

            var smtpCheck = Monosoft.Microservice.V2.SmtpCheck.CheckIfSmtpWorking(value.smtpServerName, value.smtpPort);
            if (!smtpCheck)
            {
                throw new System.ComponentModel.DataAnnotations.ValidationException("Smpt server not working");
            }

            System.IO.File.WriteAllText(settingsfile, json);
        }
    }
}
